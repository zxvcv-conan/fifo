// ====================================================================================
//                                LICENSE INFORMATIONS
// ====================================================================================
// Copyright (c) 2020-2022 Pawel Piskorz
// Licensed under the Eclipse Public License 2.0
// See attached LICENSE file
// ====================================================================================


// =================================== INCLUDES =======================================

#include <stdlib.h>
#include <string.h>
#include "fifo.h"
#ifdef _ZXVCV_USE_INTERRUPTS
    #include "interrupts.h"
#endif // _ZXVCV_USE_INTERRUPTS


// ============================== PUBLIC DEFINITIONS ==================================

#ifdef _ZXVCV_FIFO_MODE_NONCOPY

    Std_Err fifo_push_NC(Fifo_NC* self, void* val)
    {
        if (self == NULL)
        {
            return STD_REFERENCE_ERROR;
        }

        if(self->size >= self->max_size)
        {
            return STD_SIZE_OVERFLOW_ERROR;
        }

        if (self->size == 0)
        {
            #ifdef _ZXVCV_USE_INTERRUPTS
                irq_disable();
            #endif // _ZXVCV_USE_INTERRUPTS

            self->begin = (Fifo_memb*)malloc(sizeof(Fifo_memb));

            #ifdef _ZXVCV_USE_INTERRUPTS
                irq_enable();
            #endif // _ZXVCV_USE_INTERRUPTS

            if(self->begin == NULL)
            {
                return STD_ALLOC_ERROR;
            }

            self->begin->next = NULL;
            self->begin->data = val;
            self->begin->dataSize = 0;
            self->size += 1;

            return STD_OK;
        }

        Fifo_memb* prev = NULL;
        Fifo_memb* memb = self->begin;
        while (memb != NULL)
        {
            prev = memb;
            memb = memb->next;
        }

        #ifdef _ZXVCV_USE_INTERRUPTS
            irq_disable();
        #endif // _ZXVCV_USE_INTERRUPTS

        prev->next = (Fifo_memb*)malloc(sizeof(Fifo_memb));

        #ifdef _ZXVCV_USE_INTERRUPTS
            irq_enable();
        #endif // _ZXVCV_USE_INTERRUPTS

        if(prev->next == NULL)
        {
            return STD_ALLOC_ERROR;
        }

        prev->next->next = NULL;
        prev->next->data = val;
        prev->next->dataSize = 0;
        self->size += 1;

        return STD_OK;
    }

    Std_Err fifo_pop_NC(Fifo_NC* self)
    {
        if (self == NULL)
        {
            return STD_REFERENCE_ERROR;
        }

        if(self->size == 0)
        {
            return STD_NO_VALUE_ERROR;
        }

        if(self->size == 1)
        {
            #ifdef _ZXVCV_USE_INTERRUPTS
                irq_disable();
            #endif // _ZXVCV_USE_INTERRUPTS

            free(self->begin);

            #ifdef _ZXVCV_USE_INTERRUPTS
                irq_enable();
            #endif // _ZXVCV_USE_INTERRUPTS

            self->begin = NULL;
            self->size = 0;

            return  STD_OK;
        }

        Fifo_memb* del = self->begin;
        self->begin = self->begin->next;

        #ifdef _ZXVCV_USE_INTERRUPTS
            irq_disable();
        #endif // _ZXVCV_USE_INTERRUPTS

        free(del);

        #ifdef _ZXVCV_USE_INTERRUPTS
            irq_enable();
        #endif // _ZXVCV_USE_INTERRUPTS

        self->size -= 1;

        return STD_OK;
    }

    Std_Err fifo_clear_NC(Fifo_NC* self)
    {
        if(self == NULL)
        {
            return STD_REFERENCE_ERROR;
        }

        Std_Err err;
        do
        {
            err = fifo_pop_NC(self);
        }
        while (err == STD_OK);

        if(err != STD_NO_VALUE_ERROR)
        {
            return err;
        }

        return STD_OK;
    }

    Std_Err fifo_delete_NC(Fifo_NC** self)
    {
        if((*self) == NULL)
        {
            return STD_REFERENCE_ERROR;
        }

        Std_Err err = fifo_clear_NC(*self);

        if(err != STD_OK)
        {
            return err;
        }

        #ifdef _ZXVCV_USE_INTERRUPTS
            irq_disable();
        #endif // _ZXVCV_USE_INTERRUPTS

        free(*self);

        #ifdef _ZXVCV_USE_INTERRUPTS
            irq_enable();
        #endif // _ZXVCV_USE_INTERRUPTS

        *self = NULL;

        return STD_OK;
    }

#endif // _ZXVCV_FIFO_MODE_NONCOPY
