Changelog
=========

0.1.12 (2022-11-02)
-------------------
- Name of parameter 'list' renamed to 'self'.
- Add missing docstrigs.
- Fix return code in fifo_clear_NC.
- Add Unittests for using fifo_NC functions.
- Unittests fixes.
- Documentation updates.

0.1.11 (2022-10-30)
-------------------
- Documentation for fifo generaeted using docmd.

0.1.10 (2022-10-18)
-------------------
- Cleanups.
- Handly written documentation v1.

0.1.9 (2022-10-12)
------------------
- Readme updates.

0.1.8 (2022-10-12)
------------------
- Building as Release only.
  Debug will be used for testing purposes (building with --build=missing on conan install).

0.1.7 (2022-10-12)
------------------
- Define name update USE_INTERRUPTS -> _ZXVCV_USE_INTERRUPTS

0.1.6 (2022-10-12)
------------------
- Add unittests.
- Add parameter max_size to fifo_create.
- Using uint16_t for fifo size and data size instead of uint8_t.

0.1.5 (2022-10-08)
------------------
- Unittests and code cleanups.

0.1.4 (2022-10-07)
------------------
- UnitTests update.
- Source update.

0.1.3 (2022-10-05)
------------------
- UnitTests update.
- Mock generation fixes.
- Source fixes.

0.1.2 (2022-09-29)
------------------
- Compilation flags update.

0.1.1 (2022-09-27)
------------------
- Requires user update.

0.1.0 (2022-09-26)
------------------
- Build system restructurization.
- Conan build settings updates.

0.0.8 (2022-09-12)
------------------
- include stdlib.h moved to .c files from .h file

0.0.7 (2022-07-11)
------------------
- Main package files moved back into root directory.
- UT running during conan package create.
- Updates for mock.
- Add build.py script for packages creating automatization with diffrent profiles.

0.0.6 (2022-07-09)
------------------
- Main package files moved into ./package directory.
- First profiles in ./profiles directory.
- UT moved from ./test to ./unittest.

0.0.5 (2022-07-09)
------------------
- Interrupts weak moved to separate package.

0.0.4 (2022-07-09)
------------------
- LICENSE update
- parametrization

0.0.3 (2022-07-07)
------------------
- Fixes for CMakeLists.
- Add Mock subdirectory

0.0.2 (2022-07-05)
------------------
- Add UT.
- CMake build system for UT.

0.0.1 (2022-07-03)
------------------
- Basic implementation.

0.0.0 (2022-06-28)
------------------
- Initial commit.
