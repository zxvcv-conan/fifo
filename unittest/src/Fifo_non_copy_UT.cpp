
#include "gtest/gtest.h"

extern "C"
{
    #include "errors.h"
    #include "fifo.h"
}

class Fifo_non_copy_UT: public ::testing::Test {
public:
    Fifo_non_copy_UT(){}
    ~Fifo_non_copy_UT(){}

    virtual void SetUp()
    {
        ASSERT_EQ(list, nullptr);

        err = fifo_create(&list, 3);

        ASSERT_EQ(err, STD_OK);
        ASSERT_EQ(list->begin, nullptr);
        ASSERT_EQ(list->size, 0);
    }

    virtual void TearDown()
    {
        err = fifo_delete_NC(&list);

        ASSERT_EQ(err, STD_OK);
        ASSERT_EQ(list, nullptr);
    }

    Fifo* list = nullptr;
    Std_Err err;
};


/************************** TESTS **************************/

TEST_F(Fifo_non_copy_UT, fifo_push_empty)
{
    uint16_t value = 5;

    err = fifo_push_NC(list, &value);

    EXPECT_EQ(err, STD_OK);
    EXPECT_NE(list->begin, nullptr);
    EXPECT_EQ(list->begin->next, nullptr);
    EXPECT_EQ(fifo_getSize(list), 1);
    EXPECT_EQ(fifo_getDataSize(list), 0);
}

TEST_F(Fifo_non_copy_UT, fifo_push_multiple_values)
{
    uint16_t value16 = 5;
    err = fifo_push_NC(list, &value16);
    EXPECT_EQ(err, STD_OK);

    value16 = 5;
    err = fifo_push_NC(list, &value16);
    EXPECT_EQ(err, STD_OK);

    EXPECT_NE(list->begin, nullptr);
    EXPECT_NE(list->begin->next, nullptr);
    EXPECT_EQ(fifo_getSize(list), 2);
    EXPECT_EQ(fifo_getDataSize(list), 0);
}

TEST_F(Fifo_non_copy_UT, fifo_push_too_many_values)
{
    uint16_t value16 = 5;
    err = fifo_push_NC(list, &value16);
    EXPECT_EQ(err, STD_OK);
    err = fifo_push_NC(list, &value16);
    EXPECT_EQ(err, STD_OK);
    err = fifo_push_NC(list, &value16);
    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(fifo_getSize(list), 3);

    err = fifo_push_NC(list, &value16);
    EXPECT_EQ(err, STD_SIZE_OVERFLOW_ERROR);

    EXPECT_EQ(fifo_getSize(list), 3);
}

TEST_F(Fifo_non_copy_UT, fifo_push_is_deep_copy)
{
    uint16_t value16 = 5;
    err = fifo_push_NC(list, &value16);
    EXPECT_EQ(err, STD_OK);

    uint16_t* value_out = nullptr;
    err = fifo_front(list, (void**)(&value_out));
    EXPECT_EQ(*value_out, value16);

    value_out = nullptr;
    value16 = 2;
    err = fifo_front(list, (void**)(&value_out));
    EXPECT_EQ(*value_out, 2);
    EXPECT_EQ(value16, 2);
}

TEST_F(Fifo_non_copy_UT, fifo_pop_empty)
{
    EXPECT_EQ(fifo_getSize(list), 0);

    err = fifo_pop_NC(list);
    EXPECT_EQ(err, STD_NO_VALUE_ERROR);
    EXPECT_EQ(fifo_getSize(list), 0);
}

TEST_F(Fifo_non_copy_UT, fifo_pop_single_value)
{
    uint16_t value = 5;
    err = fifo_push_NC(list, &value);
    EXPECT_EQ(fifo_getSize(list), 1);

    EXPECT_NE(list->begin, nullptr);
    EXPECT_EQ(list->begin->next, nullptr);
    err = fifo_pop_NC(list);
    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(fifo_getSize(list), 0);
    EXPECT_EQ(list->begin, nullptr);

    err = fifo_pop_NC(list);
    EXPECT_EQ(fifo_getSize(list), 0);
    EXPECT_EQ(err, STD_NO_VALUE_ERROR);
}

TEST_F(Fifo_non_copy_UT, fifo_pop_multiple_values)
{
    uint16_t value = 5;
    err = fifo_push_NC(list, &value);
    err = fifo_push_NC(list, &value);
    EXPECT_EQ(fifo_getSize(list), 2);

    EXPECT_NE(list->begin, nullptr);
    EXPECT_NE(list->begin->next, nullptr);
    err = fifo_pop_NC(list);
    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(fifo_getSize(list), 1);
    EXPECT_NE(list->begin, nullptr);

    err = fifo_pop_NC(list);
    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(fifo_getSize(list), 0);

    err = fifo_pop_NC(list);
    EXPECT_EQ(fifo_getSize(list), 0);
    EXPECT_EQ(err, STD_NO_VALUE_ERROR);
}

TEST_F(Fifo_non_copy_UT, fifo_clear_empty)
{
    EXPECT_EQ(fifo_getSize(list), 0);

    err = fifo_clear_NC(list);
    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(fifo_getSize(list), 0);
}

TEST_F(Fifo_non_copy_UT, fifo_clear_single_value)
{
    uint16_t value = 5;
    err = fifo_push_NC(list, &value);
    EXPECT_EQ(fifo_getSize(list), 1);

    EXPECT_NE(list->begin, nullptr);
    EXPECT_EQ(list->begin->next, nullptr);
    err = fifo_clear_NC(list);
    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(fifo_getSize(list), 0);
    EXPECT_EQ(list->begin, nullptr);

    err = fifo_clear_NC(list);
    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(fifo_getSize(list), 0);
    EXPECT_EQ(list->begin, nullptr);
}

TEST_F(Fifo_non_copy_UT, fifo_clear_multiple_values)
{
    uint16_t value = 5;
    err = fifo_push_NC(list, &value);
    err = fifo_push_NC(list, &value);
    EXPECT_EQ(fifo_getSize(list), 2);

    EXPECT_NE(list->begin, nullptr);
    EXPECT_NE(list->begin->next, nullptr);
    err = fifo_clear_NC(list);
    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(fifo_getSize(list), 0);
    EXPECT_EQ(list->begin, nullptr);

    err = fifo_clear_NC(list);
    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(fifo_getSize(list), 0);
    EXPECT_EQ(list->begin, nullptr);
}
