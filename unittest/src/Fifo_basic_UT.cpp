
#include "gtest/gtest.h"

extern "C"
{
    #include "errors.h"
    #include "fifo.h"
}

class Fifo_basic_UT: public ::testing::Test {
public:
    Fifo_basic_UT(){}
    ~Fifo_basic_UT(){}

    virtual void SetUp()
    {
        ASSERT_EQ(list, nullptr);

        err = fifo_create(&list, INT16_MAX);

        ASSERT_EQ(err, STD_OK);
        ASSERT_EQ(list->begin, nullptr);
        ASSERT_EQ(list->size, 0);
    }

    virtual void TearDown()
    {
        err = fifo_delete_C(&list);

        ASSERT_EQ(err, STD_OK);
        ASSERT_EQ(list, nullptr);
    }

    Fifo* list = NULL;
    Std_Err err;
};


/************************** TESTS **************************/

TEST_F(Fifo_basic_UT, fifo_front_empty)
{
    int* data = nullptr;
    err = fifo_front(list, (void**)(&data));

    EXPECT_EQ(err, STD_VALUE_ERROR);
    EXPECT_EQ(data, nullptr);
}

TEST_F(Fifo_basic_UT, fifo_front_with_value)
{
    int value = 5;
    err = fifo_push_C(list, &value, sizeof(value));

    int* data = nullptr;
    err = fifo_front(list, (void**)(&data));

    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(*data, 5);
    EXPECT_EQ(fifo_getSize(list), 1);
}

TEST_F(Fifo_basic_UT, fifo_getSize_empty)
{
    uint16_t size = fifo_getSize(list);

    EXPECT_EQ(size, 0);
}

TEST_F(Fifo_basic_UT, fifo_getSize_with_one_value)
{
    int value = 5;
    err = fifo_push_C(list, &value, sizeof(value));
    EXPECT_EQ(err, STD_OK);

    uint16_t size = fifo_getSize(list);

    EXPECT_EQ(size, 1);
}

TEST_F(Fifo_basic_UT, fifo_getSize_with_many_values)
{
    int value = 5;
    err = fifo_push_C(list, &value, sizeof(value));
    EXPECT_EQ(err, STD_OK);
    err = fifo_push_C(list, &value, sizeof(value));
    EXPECT_EQ(err, STD_OK);
    err = fifo_push_C(list, &value, sizeof(value));
    EXPECT_EQ(err, STD_OK);

    uint16_t size = fifo_getSize(list);

    EXPECT_EQ(size, 3);
}

TEST_F(Fifo_basic_UT, fifo_getDataSize_empty)
{
    int size = fifo_getDataSize(list);

    EXPECT_EQ(size, 0);
}

TEST_F(Fifo_basic_UT, fifo_getDataSize_with_one_value)
{
    int value = 5;
    err = fifo_push_C(list, &value, sizeof(value));
    EXPECT_EQ(err, STD_OK);

    uint16_t size = fifo_getDataSize(list);

    EXPECT_EQ(size, sizeof(int));
}

TEST_F(Fifo_basic_UT, fifo_getDataSize_with_many_values)
{
    int valuei = 5;
    err = fifo_push_C(list, &valuei, sizeof(valuei));
    EXPECT_EQ(err, STD_OK);
    float valuef = 2.4;
    err = fifo_push_C(list, &valuef, sizeof(valuef));
    EXPECT_EQ(err, STD_OK);
    uint16_t valueui = 30;
    err = fifo_push_C(list, &valueui, sizeof(valueui));
    EXPECT_EQ(err, STD_OK);

    uint16_t size = fifo_getDataSize(list);
    EXPECT_EQ(size, sizeof(int));
    err = fifo_pop_C(list);
    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(fifo_getSize(list), 2);

    size = fifo_getDataSize(list);
    EXPECT_EQ(size, sizeof(float));
    err = fifo_pop_C(list);
    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(fifo_getSize(list), 1);

    size = fifo_getDataSize(list);
    EXPECT_EQ(size, sizeof(uint16_t));
    err = fifo_pop_C(list);
    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(fifo_getSize(list), 0);
}
