
#include "gtest/gtest.h"

extern "C"
{
    #include "errors.h"
    #include "fifo.h"
}

class Fifo_creation_UT: public ::testing::Test {
public:
    Fifo_creation_UT(){}
    ~Fifo_creation_UT(){}

    virtual void SetUp(){}

    virtual void TearDown(){}

    Fifo* list = NULL;
    Std_Err err;
};


/************************** TESTS **************************/

TEST_F(Fifo_creation_UT, fifo_create)
{
    EXPECT_EQ(list, nullptr);

    err = fifo_create(&list, INT16_MAX);

    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(list->begin, nullptr);
    EXPECT_EQ(list->size, 0);

    err = fifo_delete_NC(&list);

    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(list, nullptr);
}

TEST_F(Fifo_creation_UT, fifo_create_already_created)
{
    EXPECT_EQ(list, nullptr);

    err = fifo_create(&list, INT16_MAX);

    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(list->begin, nullptr);
    EXPECT_EQ(list->size, 0);

    err = fifo_create(&list, INT16_MAX);

    EXPECT_EQ(err, STD_REFERENCE_ERROR);
    EXPECT_NE(list, nullptr);
    EXPECT_EQ(list->begin, nullptr);
    EXPECT_EQ(list->size, 0);

    err = fifo_delete_NC(&list);

    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(list, nullptr);
}

TEST_F(Fifo_creation_UT, fifo_front_not_created)
{
    int* data = nullptr;
    err = fifo_front(list, (void**)(&data));

    EXPECT_EQ(err, STD_REFERENCE_ERROR);
    EXPECT_EQ(data, nullptr);
}

TEST_F(Fifo_creation_UT, fifo_getSize_not_created)
{
    uint8_t size;

    EXPECT_EQ(list, nullptr);

    size = fifo_getSize(list);

    EXPECT_EQ(size, 0);
}

TEST_F(Fifo_creation_UT, fifo_getDataSize_not_created)
{
    uint8_t size;

    EXPECT_EQ(list, nullptr);

    size = fifo_getDataSize(list);

    EXPECT_EQ(size, 0);
}

TEST_F(Fifo_creation_UT, fifo_push_C_not_created)
{
    uint8_t value = 5;

    EXPECT_EQ(list, nullptr);

    err = fifo_push_C(list, &value, sizeof(value));

    EXPECT_EQ(list, nullptr);
    EXPECT_EQ(err, STD_REFERENCE_ERROR);
}

TEST_F(Fifo_creation_UT, fifo_pop_C_not_created)
{
    EXPECT_EQ(list, nullptr);

    err = fifo_pop_C(list);

    EXPECT_EQ(list, nullptr);
    EXPECT_EQ(err, STD_REFERENCE_ERROR);
}

TEST_F(Fifo_creation_UT, fifo_clear_C_not_created)
{
    EXPECT_EQ(list, nullptr);

    err = fifo_clear_C(list);

    EXPECT_EQ(list, nullptr);
    EXPECT_EQ(err, STD_REFERENCE_ERROR);
}

TEST_F(Fifo_creation_UT, fifo_delete_C_not_created)
{
    EXPECT_EQ(list, nullptr);

    err = fifo_delete_C(&list);

    EXPECT_EQ(list, nullptr);
    EXPECT_EQ(err, STD_REFERENCE_ERROR);
}

TEST_F(Fifo_creation_UT, fifo_delete_C_empty)
{
    err = fifo_create(&list, 3);

    EXPECT_EQ(fifo_getSize(list), 0);

    err = fifo_delete_C(&list);
    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(list, nullptr);
}

TEST_F(Fifo_creation_UT, fifo_delete_C_single_value)
{
    err = fifo_create(&list, 3);

    uint16_t value = 5;
    err = fifo_push_C(list, &value, sizeof(value));
    EXPECT_EQ(fifo_getSize(list), 1);

    EXPECT_NE(list->begin, nullptr);
    EXPECT_EQ(list->begin->next, nullptr);
    err = fifo_delete_C(&list);
    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(list, nullptr);
}

TEST_F(Fifo_creation_UT, fifo_delete_C_multiple_values)
{
    err = fifo_create(&list, 3);

    uint16_t value = 5;
    err = fifo_push_C(list, &value, sizeof(value));
    err = fifo_push_C(list, &value, sizeof(value));
    EXPECT_EQ(fifo_getSize(list), 2);

    EXPECT_NE(list->begin, nullptr);
    EXPECT_NE(list->begin->next, nullptr);
    err = fifo_delete_C(&list);
    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(list, nullptr);
}

TEST_F(Fifo_creation_UT, fifo_push_NC_not_created)
{
    uint8_t value = 5;

    EXPECT_EQ(list, nullptr);

    err = fifo_push_NC(list, &value);

    EXPECT_EQ(list, nullptr);
    EXPECT_EQ(err, STD_REFERENCE_ERROR);
}

TEST_F(Fifo_creation_UT, fifo_pop_NC_not_created)
{
    EXPECT_EQ(list, nullptr);

    err = fifo_pop_NC(list);

    EXPECT_EQ(list, nullptr);
    EXPECT_EQ(err, STD_REFERENCE_ERROR);
}

TEST_F(Fifo_creation_UT, fifo_clear_NC_not_created)
{
    EXPECT_EQ(list, nullptr);

    err = fifo_clear_NC(list);

    EXPECT_EQ(list, nullptr);
    EXPECT_EQ(err, STD_REFERENCE_ERROR);
}

TEST_F(Fifo_creation_UT, fifo_delete_NC_not_created)
{
    EXPECT_EQ(list, nullptr);

    err = fifo_delete_NC(&list);

    EXPECT_EQ(list, nullptr);
    EXPECT_EQ(err, STD_REFERENCE_ERROR);
}

TEST_F(Fifo_creation_UT, fifo_delete_NC_empty)
{
    err = fifo_create(&list, 3);

    EXPECT_EQ(fifo_getSize(list), 0);

    err = fifo_delete_NC(&list);
    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(list, nullptr);
}

TEST_F(Fifo_creation_UT, fifo_delete_NC_single_value)
{
    err = fifo_create(&list, 3);

    uint16_t value = 5;
    err = fifo_push_NC(list, &value);
    EXPECT_EQ(fifo_getSize(list), 1);

    EXPECT_NE(list->begin, nullptr);
    EXPECT_EQ(list->begin->next, nullptr);
    err = fifo_delete_NC(&list);
    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(list, nullptr);
}

TEST_F(Fifo_creation_UT, fifo_delete_NC_multiple_values)
{
    err = fifo_create(&list, 3);

    uint16_t value = 5;
    err = fifo_push_NC(list, &value);
    err = fifo_push_NC(list, &value);
    EXPECT_EQ(fifo_getSize(list), 2);

    EXPECT_NE(list->begin, nullptr);
    EXPECT_NE(list->begin->next, nullptr);
    err = fifo_delete_NC(&list);
    EXPECT_EQ(err, STD_OK);
    EXPECT_EQ(list, nullptr);
}
