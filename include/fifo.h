// ====================================================================================
//                                LICENSE INFORMATIONS
// ====================================================================================
// Copyright (c) 2020-2022 Pawel Piskorz
// Licensed under the Eclipse Public License 2.0
// See attached LICENSE file
// ====================================================================================


#ifndef ZXVCV_FIFO_H_
#define ZXVCV_FIFO_H_

// =================================== INCLUDES =======================================
#include <stdint.h>
#include "errors.h"

// ================================== DATA TYPES ======================================

/** Fifo member queue type.
 *
 * Should not been used directly by a user. For fifo component internal use only.
 */
typedef
/** Fifo member queue object.
 *
 * Object that is a single node for the item in the queue.<br>
 * This object store information, about single added item, and link between them.<br>
 * Its created and deleted dynamically during fifo methods usage.<br>
 * Should not been used directly by a user.
 */
struct Fifo_memb_Tag {
    /** Pointer to the next item in the queue (last item - NULL). */
    struct Fifo_memb_Tag* next;

    /** Pointer to the data item. */
    void* data;

    /** Size in Bytes of the data item. If NC queue is used, always 0. */
    uint16_t dataSize;
} Fifo_memb;

/** Fifo queue type.
 *
 * Should not been used directly. Use Fifo_C and Fifo_NC typedefs instead.
 */
typedef
/** Fifo queue object.
 *
 * Main object passed into all fifo component functions.<br>
 * This object store information, that should persist between methods invocations.<br>
 * First action should alway be fifo_create.<br>
 * Last action should always be fifo_delete_C or fifo_delete_NC.<br>
 */
struct Fifo_Tag {
    /** Pointer to the queue head item. */
    Fifo_memb* begin;

    /** Queue size counter. */
    uint16_t size;

    /** Maximum number of objects that could be stored in the queue. */
    uint16_t max_size;
} Fifo;

#ifdef _ZXVCV_FIFO_MODE_COPY
    /** Fifo queue Copy type.
     *
     * This type should be used to mark, that this instance of fifo will be used with _C methods only.
     * Cannot be mixed with Fifo_NC queue.
     */
    typedef Fifo    Fifo_C;
#endif // _ZXVCV_FIFO_MODE_COPY

#ifdef _ZXVCV_FIFO_MODE_NONCOPY
    /** Fifo queue NonCopy type.
     *
     * This type should be used to mark, that this instance of fifo will be used with _NC methods only.
     * Cannot be mixed with Fifo_C queue.
     */
    typedef Fifo    Fifo_NC;
#endif // _ZXVCV_FIFO_MODE_NONCOPY


// ============================== PUBLIC DECLARATIONS =================================

/** Initialize fifo.
 *
 * Allocates memory for data structure, which will be used tho store information, that should persist between methods invocations.
 * Every use of 'fifo_create' require use the right kind of queue delete (fifo_delete_C or fifo_delete_NC) - depends which queue will be used with this object.
 *
 * @param self [out] Placeholder for created fifo object.
 * @param max_size [in] The maximum number of items that a queue can store at once.
 *
 * @return An enum object that contains the exit code.
 * STD_OK Execution success.
 * STD_REFERENCE_ERROR Passed self placeholder is not NULL (already initialized).
 * STD_ALLOC_ERROR Memory allocation failed.
 */
Std_Err fifo_create(Fifo** self, uint16_t max_size);

/** Get front item from the queue.
 *
 * If there are no values in queue error code will be returned.
 *
 * @param self [in] Fifo object.
 * @param data [in] Placeholder for returned value. Should be initialized to NULL.
 *
 * @return An enum object that contains the exit code.
 * STD_OK Execution success.
 * STD_REFERENCE_ERROR Passed self placeholder is NULL (not initialized).
 * STD_VALUE_ERROR No value to return - fifo is empty.
 */
Std_Err fifo_front(Fifo* self, void** data);

/** Return current size of the queue.
 *
 * @param self [in] Fifo object.
 * @return Size of the queue. 0 could mean that queue is empty or queue was not been initialized.
 */
uint16_t fifo_getSize(Fifo* self);

/** Return data size of the front item front from the queue.
 *
 * @param self [in] Fifo object.
 * @return Size of the front item in Bytes. 0 could mean that queue is empty or queue was not initialized.
 */
uint16_t fifo_getDataSize(Fifo* self);

#ifdef _ZXVCV_FIFO_MODE_COPY
    /** Add val to the end of the queue.
     *
     * If there queue is full, error code will be returned.
     * valSize is used to allocate memory for passed value, so its value should match
     * the result of sizeof(<type_of_val>).
     * This type of queue will create a deep copy of the 'val' and will store in the queue
     * separate copy of the item, instead of passed source object.
     *
     * @param self [in] Fifo object.
     * @param val [in] Value to add.
     * @param valSize [in] Size of the val in Bytes.
     *
     * @return An enum object that contains the exit code.
     * STD_OK Execution success.
     * STD_REFERENCE_ERROR Passed self placeholder is NULL (not initialized).
     * STD_SIZE_OVERFLOW_ERROR Queue is full.
     * STD_ALLOC_ERROR Memory allocation failed.
     */
    Std_Err fifo_push_C(Fifo_C* self, void* val, int valSize);

    /** Remove first val from the queue.
     *
     * If there queue is empty, error code will be returned.
     *
     * @param self [in] Fifo object.
     *
     * @return An enum object that contains the exit code.
     * STD_OK Execution success.
     * STD_REFERENCE_ERROR Passed self placeholder is NULL (not initialized).
     * STD_NO_VALUE_ERROR Queue is empty.
     */
    Std_Err fifo_pop_C(Fifo_C* self);

    /** Remove all values from the queue.
     *
     * @param self [in] Fifo object.
     *
     * @return An enum object that contains the exit code.
     * STD_OK Execution success.
     * STD_REFERENCE_ERROR Passed list placeholder is NULL (not initialized).
     */
    Std_Err fifo_clear_C(Fifo_C* self);

    /** Deinitialize the queue.
     *
     * Remove all values from the queue, and remove queue itself.
     *
     * @param self [in] Fifo object.
     *
     * @return An enum object that contains the exit code.
     * STD_OK Execution success.
     * STD_REFERENCE_ERROR Passed self placeholder is NULL (not initialized).
     */
    Std_Err fifo_delete_C(Fifo_C** self);
#endif // _ZXVCV_FIFO_MODE_COPY

#ifdef _ZXVCV_FIFO_MODE_NONCOPY
    /** Add val to the end of the queue.
     *
     * If there queue is full, error code will be returned.
     * This type of queue will store in the queue pointer to the passed source object.
     *
     * @param self [in] Fifo object.
     * @param val [in] Value to add.
     *
     * @return An enum object that contains the exit code.
     * STD_OK Execution success.
     * STD_REFERENCE_ERROR Passed self placeholder is NULL (not initialized).
     * STD_SIZE_OVERFLOW_ERROR Queue is full.
     * STD_ALLOC_ERROR Memory allocation failed.
     */
    Std_Err fifo_push_NC(Fifo_NC* self, void* val);

    /** Remove first val from the queue.
     *
     * If there queue is empty, error code will be returned.
     *
     * @param self [in] Fifo object.
     *
     * @return An enum object that contains the exit code.
     * STD_OK Execution success.
     * STD_REFERENCE_ERROR Passed self placeholder is NULL (not initialized).
     * STD_NO_VALUE_ERROR Queue is empty.
     */
    Std_Err fifo_pop_NC(Fifo_NC* self);

    /** Remove all values from the queue.
     *
     * @param self [in] Fifo object.
     *
     * @return An enum object that contains the exit code.
     * STD_OK Execution success.
     * STD_REFERENCE_ERROR Passed list placeholder is NULL (not initialized).
     */
    Std_Err fifo_clear_NC(Fifo_NC* self);

    /** Deinitialize the queue.
     *
     * Remove all values from the queue, and remove queue itself.
     *
     * @param self [in] Fifo object.
     *
     * @return An enum object that contains the exit code.
     * STD_OK Execution success.
     * STD_REFERENCE_ERROR Passed self placeholder is NULL (not initialized).
     */
    Std_Err fifo_delete_NC(Fifo_NC** self);
#endif // _ZXVCV_FIFO_MODE_NONCOPY

#endif // ZXVCV_FIFO_H_
