


# API

[comment]: # ([[_DOCMD_USER_BLOCK_0_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_0_END]])

</br>

## Structures
***

[comment]: # ([[_DOCMD_USER_BLOCK_1_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_1_END]])

> ## Fifo_memb_Tag
> ***
> Fifo member queue object.
>
> Object that is a single node for the item in the queue.<br>This object store information, about single added item, and link between them.<br>Its created and deleted dynamically during fifo methods usage.<br>Should not been used directly by a user.
>
> ### <u><i>Members:</i></u>
> |NAME|TYPE|DESCRIPTION|
> |-|-|-|
> |next|Fifo_memb_Tag*|Pointer to the next item in the queue (last item - NULL).|
> |data|void*|Pointer to the data item.|
> |dataSize|uint16_t|Size in Bytes of the data item. If NC queue is used, always 0.|

</br>

> ## Fifo_Tag
> ***
> Fifo queue object.
>
> Main object passed into all fifo component functions.<br>This object store information, that should persist between methods invocations.<br>First action should alway be fifo_create.<br>Last action should always be fifo_delete_C or fifo_delete_NC.<br>
>
> ### <u><i>Members:</i></u>
> |NAME|TYPE|DESCRIPTION|
> |-|-|-|
> |begin|Fifo_memb*|Pointer to the queue head item.|
> |size|uint16_t|Queue size counter.|
> |max_size|uint16_t|Maximum number of objects that could be stored in the queue.|

</br>





## Typedefs
***

[comment]: # ([[_DOCMD_USER_BLOCK_4_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_4_END]])

> ## Fifo_memb
> ***
> Fifo member queue type.
>
> ### <u><i>Definition:</i></u>
> Fifo_memb_Tag
>
> ### <u><i>Description:</i></u>
> Should not been used directly by a user. For fifo component internal use only.
>

</br>

> ## Fifo
> ***
> Fifo queue type.
>
> ### <u><i>Definition:</i></u>
> Fifo_Tag
>
> ### <u><i>Description:</i></u>
> Should not been used directly. Use Fifo_C and Fifo_NC typedefs instead.
>

</br>

> ## Fifo_C
> ***
> Fifo queue Copy type.
>
> ### <u><i>Definition:</i></u>
> Fifo
>
> ### <u><i>Description:</i></u>
> This type should be used to mark, that this instance of fifo will be used with _C methods only.Cannot be mixed with Fifo_NC queue.
>

</br>

> ## Fifo_NC
> ***
> Fifo queue Copy type.
>
> ### <u><i>Definition:</i></u>
> Fifo
>
> ### <u><i>Description:</i></u>
> This type should be used to mark, that this instance of fifo will be used with _C methods only.Cannot be mixed with Fifo_NC queue.
>

</br>



</br>

## Functions
***

[comment]: # ([[_DOCMD_USER_BLOCK_5_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_5_END]])

> ## fifo_create
> ***
> Initialize fifo.
>
> Allocates memory for data structure, which will be used tho store information, that should persist between methods invocations.Every use of 'fifo_create' require use the right kind of queue delete (fifo_delete_C or fifo_delete_NC) - depends which queue will be used with this object.
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |self|Fifo**|out|Placeholder for created fifo object.|
> |max_size|uint16_t|in|The maximum number of items that a queue can store at once.|
> ### <u><i>Return:</i></u>
> (Std_Err) An enum object that contains the exit code.
> |CODE|DESCRIPTION|
> |-|-|
> |STD_OK|Execution success.|
> |STD_REFERENCE_ERROR|Passed self placeholder is not NULL (already initialized).|
> |STD_ALLOC_ERROR|Memory allocation failed.|

</br>

> ## fifo_front
> ***
> Get front item from the queue.
>
> If there are no values in queue error code will be returned.
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |self|Fifo*|in|Fifo object.|
> |data|void**|in|Placeholder for returned value. Should be initialized to NULL.|
> ### <u><i>Return:</i></u>
> (Std_Err) An enum object that contains the exit code.
> |CODE|DESCRIPTION|
> |-|-|
> |STD_OK|Execution success.|
> |STD_REFERENCE_ERROR|Passed self placeholder is NULL (not initialized).|
> |STD_VALUE_ERROR|No value to return - fifo is empty.|

</br>

> ## fifo_getSize
> ***
> Return current size of the queue.
>
> 
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |self|Fifo*|in|Fifo object.|
> ### <u><i>Return:</i></u>
> (uint16_t) Size of the queue. 0 could mean that queue is empty or queue was not been initialized.

</br>

> ## fifo_getDataSize
> ***
> Return data size of the front item front from the queue.
>
> 
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |self|Fifo*|in|Fifo object.|
> ### <u><i>Return:</i></u>
> (uint16_t) Size of the front item in Bytes. 0 could mean that queue is empty or queue was not initialized.

</br>

> ## fifo_push_C
> ***
> Add val to the end of the queue.
>
> If there queue is full, error code will be returned.valSize is used to allocate memory for passed value, so its value should matchthe result of sizeof(<type_of_val>).This type of queue will create a deep copy of the 'val' and will store in the queueseparate copy of the item, instead of passed source object.
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |self|Fifo_C*|in|Fifo object.|
> |val|void*|in|Value to add.|
> |valSize|int|in|Size of the val in Bytes.|
> ### <u><i>Return:</i></u>
> (Std_Err) An enum object that contains the exit code.
> |CODE|DESCRIPTION|
> |-|-|
> |STD_OK|Execution success.|
> |STD_REFERENCE_ERROR|Passed self placeholder is NULL (not initialized).|
> |STD_SIZE_OVERFLOW_ERROR|Queue is full.|
> |STD_ALLOC_ERROR|Memory allocation failed.|

</br>

> ## fifo_pop_C
> ***
> Remove first val from the queue.
>
> If there queue is empty, error code will be returned.
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |self|Fifo_C*|in|Fifo object.|
> ### <u><i>Return:</i></u>
> (Std_Err) An enum object that contains the exit code.
> |CODE|DESCRIPTION|
> |-|-|
> |STD_OK|Execution success.|
> |STD_REFERENCE_ERROR|Passed self placeholder is NULL (not initialized).|
> |STD_NO_VALUE_ERROR|Queue is empty.|

</br>

> ## fifo_clear_C
> ***
> Remove all values from the queue.
>
> 
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |self|Fifo_C*|in|Fifo object.|
> ### <u><i>Return:</i></u>
> (Std_Err) An enum object that contains the exit code.
> |CODE|DESCRIPTION|
> |-|-|
> |STD_OK|Execution success.|
> |STD_REFERENCE_ERROR|Passed list placeholder is NULL (not initialized).|

</br>

> ## fifo_delete_C
> ***
> Deinitialize the queue.
>
> Remove all values from the queue, and remove queue itself.
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |self|Fifo_C**|in|Fifo object.|
> ### <u><i>Return:</i></u>
> (Std_Err) An enum object that contains the exit code.
> |CODE|DESCRIPTION|
> |-|-|
> |STD_OK|Execution success.|
> |STD_REFERENCE_ERROR|Passed self placeholder is NULL (not initialized).|

</br>

> ## fifo_push_NC
> ***
> Add val to the end of the queue.
>
> If there queue is full, error code will be returned.This type of queue will store in the queue pointer to the passed source object.
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |self|Fifo_NC*|in|Fifo object.|
> |val|void*|in|Value to add.|
> ### <u><i>Return:</i></u>
> (Std_Err) An enum object that contains the exit code.
> |CODE|DESCRIPTION|
> |-|-|
> |STD_OK|Execution success.|
> |STD_REFERENCE_ERROR|Passed self placeholder is NULL (not initialized).|
> |STD_SIZE_OVERFLOW_ERROR|Queue is full.|
> |STD_ALLOC_ERROR|Memory allocation failed.|

</br>

> ## fifo_pop_NC
> ***
> Remove first val from the queue.
>
> If there queue is empty, error code will be returned.
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |self|Fifo_NC*|in|Fifo object.|
> ### <u><i>Return:</i></u>
> (Std_Err) An enum object that contains the exit code.
> |CODE|DESCRIPTION|
> |-|-|
> |STD_OK|Execution success.|
> |STD_REFERENCE_ERROR|Passed self placeholder is NULL (not initialized).|
> |STD_NO_VALUE_ERROR|Queue is empty.|

</br>

> ## fifo_clear_NC
> ***
> Remove all values from the queue.
>
> 
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |self|Fifo_NC*|in|Fifo object.|
> ### <u><i>Return:</i></u>
> (Std_Err) An enum object that contains the exit code.
> |CODE|DESCRIPTION|
> |-|-|
> |STD_OK|Execution success.|
> |STD_REFERENCE_ERROR|Passed list placeholder is NULL (not initialized).|

</br>

> ## fifo_delete_NC
> ***
> Deinitialize the queue.
>
> Remove all values from the queue, and remove queue itself.
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |self|Fifo_NC**|in|Fifo object.|
> ### <u><i>Return:</i></u>
> (Std_Err) An enum object that contains the exit code.
> |CODE|DESCRIPTION|
> |-|-|
> |STD_OK|Execution success.|
> |STD_REFERENCE_ERROR|Passed self placeholder is NULL (not initialized).|

</br>



[comment]: # ([[_DOCMD_USER_BLOCK_6_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_6_END]])

***
[Main page](../README.md) | [Repository url](https://gitlab.com/zxvcv-conan/fifo) </br>
***
<i>Generated with <b>docmd</b> Python package.</i>
