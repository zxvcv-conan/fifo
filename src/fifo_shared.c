// ====================================================================================
//                                LICENSE INFORMATIONS
// ====================================================================================
// Copyright (c) 2020-2022 Pawel Piskorz
// Licensed under the Eclipse Public License 2.0
// See attached LICENSE file
// ====================================================================================


// =================================== INCLUDES =======================================

#include <stdlib.h>
#include <string.h>
#include "fifo.h"
#ifdef _ZXVCV_USE_INTERRUPTS
    #include "interrupts.h"
#endif // _ZXVCV_USE_INTERRUPTS


// ============================== PUBLIC DEFINITIONS ==================================

Std_Err fifo_create(Fifo** self, uint16_t max_size)
{
    if ((*self) != NULL)
    {
        return STD_REFERENCE_ERROR;
    }

    #ifdef _ZXVCV_USE_INTERRUPTS
        irq_disable();
    #endif // _ZXVCV_USE_INTERRUPTS

    *self = (Fifo*)malloc(sizeof(Fifo));

    #ifdef _ZXVCV_USE_INTERRUPTS
        irq_enable();
    #endif // _ZXVCV_USE_INTERRUPTS

    if(*self == NULL)
    {
        return STD_ALLOC_ERROR;
    }

    (*self)->begin = NULL;
    (*self)->size = 0;
    (*self)->max_size = max_size;
    return STD_OK;
}

Std_Err fifo_front(Fifo* self, void** data)
{
    if (self == NULL)
    {
        return STD_REFERENCE_ERROR;
    }

    if(self->size == 0)
    {
        (*data) = NULL;
        return STD_VALUE_ERROR;
    }

    (*data) = self->begin->data;
    return STD_OK;
}

uint16_t fifo_getSize(Fifo* self)
{
    if (self == NULL)
    {
        return 0;
    }

    return self->size;
}

uint16_t fifo_getDataSize(Fifo* self)
{
    if (self == NULL)
    {
        return 0;
    }

    if(self->size <= 0)
    {
        return 0;
    }

    return self->begin->dataSize;
}
