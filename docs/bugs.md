


# Known Bugs

[comment]: # ([[_DOCMD_USER_BLOCK_0_BEGIN]])
Not working (its a gitlab bug: https://gitlab.com/gitlab-org/gitlab/-/issues/329307)
```
conan install "fifo/[<1.0.0]@zxvcv-conan+fifo/testing" -r gitlab --build=missing -s build_type=Debug
```
```
conan install .. -r gitlab --build=missing -s build_type=Debug
```

But this is working:
```
conan install "errors/0.1.1@zxvcv-conan+errors/testing" -r gitlab -s build_type=Debug --build=missing
conan install "interrupts/0.1.1@zxvcv-conan+interrupts/testing" -r gitlab -s build_type=Debug --build=missing
conan install "fifo/0.1.9@zxvcv-conan+fifo/testing" -r gitlab -s build_type=Debug --build=missing
```


```
https://gitlab.com/api/v4/packages/conan/v1/conans/search?q=fifo%2F%2A%40zxvcv-conan%2Bfifo%2Ftesting
https://gitlab.com/api/v4/packages/conan/v1/conans/search?q=fifo%40zxvcv-conan%2Bfifo%2Ftesting

https://gitlab.com/api/v4/packages/conan/v1/conans/searchq=fifo/0.1.9@zxvcv-conan+fifo/testing
https://gitlab.com/api/v4/packages/conan/v1/conans/searchq=fifo%2F0.1.9%40zxvcv-conan%2Bfifo%2Ftesting
```

[comment]: # ([[_DOCMD_USER_BLOCK_0_END]])

[comment]: # ([[_DOCMD_USER_BLOCK_1_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_1_END]])

***
[Main page](../README.md) | [Repository url](https://gitlab.com/zxvcv-conan/fifo) </br>
***
<i>Generated with <b>docmd</b> Python package.</i>
